<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function() use ($router) {
    $router->post('/login', 'AuthController@login');
    $router->post('/register', 'AuthController@register');
    $router->group(['middleware' => 'auth'], function() use ($router) {
        $router->post('/logout', 'AuthController@logout');
    });
});

$router->group(['prefix' => 'public'], function() use ($router) {
    $router->get('/product_category', 'PublicController@getProductCategory');
    $router->get('/province', 'PublicController@getProvince');
    $router->get('/news', 'PublicController@getNews');
    $router->get('/city/{id}', 'PublicController@getCity');
    $router->get('/district/{id}', 'PublicController@getDistrict');
    $router->get('/village/{id}', 'PublicController@getVillage');
});

$router->group(['prefix' => 'user','middleware' => 'auth'], function() use ($router) {
    $router->post('/{id}/merchant', 'UserController@createMerchant');
    $router->group(['prefix' => 'merchant'], function() use ($router) {
        $router->get('/', 'UserController@getMerchant');
        $router->post('/update', 'UserController@updateMerchant');
        $router->put('status/{id}', 'UserController@updateStatusMerchant');
        $router->put('siup-status/{id}', 'UserController@updateStatusSiupMerchant');
    });
    $router->get('/', 'UserController@getUser');
    $router->post('/', 'UserController@createUser');
    $router->put('/{id}', 'UserController@updateUser');
});

$router->group(['prefix' => 'product','middleware' => 'auth'], function() use ($router) {
    $router->get('/merchant/{id}', 'ProductController@getProductMerchant');
    $router->get('/', 'ProductController@getAllProduct');
    $router->delete('/{id}', 'ProductController@inActiveProduct');
});

$router->group(['prefix' => 'category','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'ProductCategoryController@getCategory');
    $router->post('/', 'ProductCategoryController@createCategory');
    $router->put('/{id}', 'ProductCategoryController@updateCategory');
    $router->delete('/{id}', 'ProductCategoryController@inActiveCategory');
});

$router->group(['prefix' => 'news','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'NewsController@getNews');
    $router->get('/{id}', 'NewsController@getNewsById');
    $router->post('/', 'NewsController@createNews');
    $router->post('/{id}', 'NewsController@updateNews');
    $router->delete('/{id}', 'NewsController@deleteNews');
});

$router->group(['prefix' => 'address','middleware' => 'auth'], function() use ($router) {
    $router->get('/province', 'AddressController@getProvince');
    $router->get('/city/{id}', 'AddressController@getCity');
    $router->get('/district/{id}', 'AddressController@getDistrict');
    $router->get('/village/{id}', 'AddressController@getVillage');
});

$router->group(['prefix' => 'payment-method','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'PaymentMethodController@index');
    $router->post('/', 'PaymentMethodController@create');
    $router->put('/{id}', 'PaymentMethodController@update');
    $router->delete('/{id}', 'PaymentMethodController@destroy');
});

$router->group(['prefix' => 'topup','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'TopUpController@getTopUp');
    $router->post('/', 'TopUpController@createTopUp');
    $router->put('/{id}', 'TopUpController@updateTopUp');
});

$router->group(['prefix' => 'provider','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'ProviderController@index');
    $router->get('/{id}', 'ProviderController@getProviderById');
    $router->post('/', 'ProviderController@create');
    $router->post('/{id}', 'ProviderController@update');
    $router->delete('/{id}', 'ProviderController@destroy');
});

$router->group(['prefix' => 'denom','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'DenomController@index');
    $router->post('/', 'DenomController@create');
    $router->post('/{id}', 'DenomController@update');
    $router->delete('/{id}', 'DenomController@destroy');
});

$router->group(['prefix' => 'admin-fee','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'AdminFeeController@index');
    $router->put('/{id}', 'AdminFeeController@update');
});

$router->group(['prefix' => 'courier','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'CourierController@index');
    $router->post('/', 'CourierController@create');
    $router->post('/{id}', 'CourierController@update');
    $router->delete('/{id}', 'CourierController@destroy');
});

$router->group(['prefix' => 'withdraw','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'WithdrawController@getWithdraw');
    $router->put('/{id}', 'WithdrawController@updateStatus');
});

$router->group(['prefix' => 'home','middleware' => 'auth'], function() use ($router) {
    $router->get('/credit', 'HomeController@index');
    $router->get('/info', 'InfoController@index');
    $router->get('/chart/digital', 'HomeController@chartTransactionDigital');
    $router->get('/chart/product', 'HomeController@chartTransactionProduct');
    $router->get('/chart/income', 'HomeController@chartIncome');
});

$router->group(['prefix' => 'transaction','middleware' => 'auth'], function() use ($router) {
    $router->get('/digital', 'TransactionController@getTransactionDigital');
    $router->get('/product', 'TransactionController@getTransactionProduct');
    $router->put('/product/{id}', 'TransactionController@updateStatus');
});

$router->group(['prefix' => 'banner','middleware' => 'auth'], function() use ($router) {
    $router->get('/', 'BannerController@index');
    $router->post('/', 'BannerController@create');
    $router->post('/{id}', 'BannerController@update');
    $router->delete('/{id}', 'BannerController@destroy');
});
