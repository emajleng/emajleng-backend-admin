<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable(false);
            $table->string('email')->nullable(false)->unique();
            $table->string('phone')->nullable(false)->unique();
            $table->text('address')->nullable(false);
            $table->bigInteger('village_id')->nullable(false);
            $table->bigInteger('district_id')->nullable(false);
            $table->bigInteger('city_id')->nullable(false);
            $table->bigInteger('province_id')->nullable(false);
            $table->bigInteger('balance')->nullable(false)->default(0);
            $table->string('fcm_token');
            $table->string('password');
            $table->string('latest_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_user');
    }
}
