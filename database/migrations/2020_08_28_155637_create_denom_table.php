<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDenomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denom', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('provider_id')->nullable(false);
            $table->bigInteger('amount')->nullable(false);
            $table->string('code')->nullable(false);
            $table->bigInteger('price')->nullable(false);
            $table->bigInteger('admin_fee')->nullable(false);
            $table->boolean('status')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denom');
    }
}
