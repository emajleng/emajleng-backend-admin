<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Client;
use App\Models\Merchant;
use App\Models\Transaction;
use App\Models\TransactionPulsa;
use App\Models\TransactionEmoney;
use App\Models\TransactionPacketData;
use App\Models\TransactionPlnPostpaid;
use App\Models\TransactionPlnPrepaid;
use App\Models\User;
use App\Models\TopUp;
use App\Helper\ResponseHelper;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function index(){
        $urlData=[
            "commands"=> 'deposit',
            'username'=>env('UsernameDigiflazz'),
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').'depo')
        ];
        $digiflazz = Client::digiflazz($urlData,'cek-saldo');
        $totalUserBalance = User::sum('balance');
        $merchant=Merchant::count();
        $user=User::count();
        $data=[
            "digiflazzCredit"=>$digiflazz->data->deposit,
            "totalUserBalance"=>(int)$totalUserBalance,
            "user"=>[
                'merchant'=>$merchant,
                'user'=>$user
            ],
            'transaction'=>$this->countTransaction(),
            'income'=>$this->getIncome(),
        ];
        return ResponseHelper::ok($data);
    }

    public function chartTransactionDigital(Request $request){
        $validator = Validator::make($request->all(), [
            'start_date'=> 'required',
            'end_date'=> 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        if(!$request->type){
            return ResponseHelper::ok($this->getAllTransaction($request->start_date,$request->end_date));
        }
        $data = $this->query($request->type);
        $transaction = $data->whereDate('created_at', '>=', $request->start_date)
            ->whereDate('created_at', '<=', $request->end_date)
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        $chart=[
            'all'=>$this->checkNullTransaction($transaction,$request->start_date,$request->end_date),
            'success'=>$this->getTransactionSucces($request->type,$request->start_date,$request->end_date),
            'failed'=>$this->getTransactionFailed($request->type,$request->start_date,$request->end_date)
        ];
        return ResponseHelper::ok($chart);
    }

    public function chartTransactionProduct(Request $request){
        $validator = Validator::make($request->all(), [
            'start_date'=> 'required',
            'end_date'=> 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        $transaction = Transaction::query()->whereDate('created_at','>=', $request->start_date)
            ->whereDate('created_at','<=', $request->end_date)
            ->where('status','LIKE','%'.$request->status.'%')
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        $chart=[
            'all'=>$this->checkNullTransaction($transaction,$request->start_date,$request->end_date),
            // 'success'=>$this->getTransactionSucces($request->type,$request->start_date,$request->end_date),
            // 'failed'=>$this->getTransactionFailed($request->type,$request->start_date,$request->end_date)
        ];
        return ResponseHelper::ok($chart);
    }

    public function chartIncome(Request $request){
        $validator = Validator::make($request->all(), [
            'start_date'=> 'required',
            'end_date'=> 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        if(!$request->type){
            return ResponseHelper::ok($this->getAllIncome($request->start_date,$request->end_date));
        }
        if($request->type=='PULSA'){
            $data = TransactionPulsa::query();
        }elseif($request->type=='DATA'){
            $data = TransactionPacketData::query();
        }elseif($request->type=='EMONEY'){
            $data = TransactionEmoney::query();
        }elseif($request->type=='PLN'){
            $data = TransactionPlnPrepaid::query();
        }elseif($request->type=='PLN_POSTPAID'){
            $data = TransactionPlnPostpaid::query();
        }
        $result=[];
        $transaction = $data->whereDate('created_at','>=', $request->start_date)
            ->whereDate('created_at','<=', $request->end_date)
            ->selectRaw('date(created_at) date')
            ->selectRaw('sum(admin_fee) as income')
            ->groupBy('date')
            ->get();
        foreach($transaction as $transaction){
            $response=[
                'date'=>$transaction->date,
                'income'=>(int)$transaction->income
            ];
            array_push($result,$response);
        }
        return ResponseHelper::ok($result);
    }

    public function countTransaction(){
        $pendingPulsa = TransactionPulsa::where('status','PENDING')->count();
        $succesPulsa = TransactionPulsa::where('status','SUKSES')->count();
        $failedPulsa = TransactionPulsa::where('status','GAGAL')->count();

        $pendingEmoney = TransactionEmoney::where('status','PENDING')->count();
        $succesEmoney = TransactionEmoney::where('status','SUKSES')->count();
        $failedEmoney = TransactionEmoney::where('status','GAGAL')->count();

        $pendingPacketData = TransactionPacketData::where('status','PENDING')->count();
        $succesPacketData = TransactionPacketData::where('status','SUKSES')->count();
        $failedPacketData = TransactionPacketData::where('status','GAGAL')->count();

        $pendingPlnPrepaid = TransactionPlnPrepaid::where('status','PENDING')->count();
        $succesPlnPrepaid = TransactionPlnPrepaid::where('status','SUKSES')->count();
        $failedPlnPrepaid = TransactionPlnPrepaid::where('status','GAGAL')->count();

        $pendingPlnPostpaid = TransactionPlnPostpaid::where('status','PENDING')->count();
        $succesPlnPostpaid = TransactionPlnPostpaid::where('status','SUKSES')->count();
        $failedPlnPostpaid = TransactionPlnPostpaid::where('status','GAGAL')->count();

        $transaction = [
            'success' => $succesEmoney+$succesPacketData+$succesPlnPrepaid+$succesPlnPostpaid+$succesPulsa,
            'pending' => $pendingEmoney+$pendingPacketData+$pendingPlnPrepaid+$pendingPlnPostpaid+$pendingPulsa,
            'failed' => $failedEmoney+$failedPacketData+$failedPlnPrepaid+$failedPlnPostpaid+$failedPulsa,
        ];
        return $transaction;
    }

    public function query($type){
        if($type=='PULSA'){
            $data = TransactionPulsa::query();
        }elseif($type=='DATA'){
            $data = TransactionPacketData::query();
        }elseif($type=='EMONEY'){
            $data = TransactionEmoney::query();
        }elseif($type=='PLN'){
            $data = TransactionPlnPrepaid::query();
        }elseif($type=='PLN_POSTPAID'){
            $data = TransactionPlnPostpaid::query();
        }
        return $data;
    }

    public function getTransactionSucces($type,$start_date,$end_date){
        $query = $this->query($type);
        $transactions=$query->where('status','Sukses')
            ->whereDate('created_at', '>=', $start_date)
            ->whereDate('created_at', '<=', $end_date)
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        return $this->checkNullTransaction($transactions,$start_date,$end_date);
    }

    public function checkNullTransaction($transactions,$start_date,$end_date){
        $start_date=Carbon::parse($start_date);
        $end_date=Carbon::parse($end_date);
        $diff = $start_date->diffInDays($end_date);
        $data = [];
        for($i = 0; $i <= $diff; $i++){
            $date=Carbon::parse($start_date)->addDays($i)->format('Y-m-d');
            $count=0;
            foreach($transactions as $transaction){
                if($date==$transaction['date']){
                    $count=$transaction['count'];
                }
            }
            $response=[
                'date'=>$date,
                'count'=>$count
            ];
            array_push($data,$response);
        }
        return $data;
    }

    public function getTransactionFailed($type,$start_date,$end_date){
        $query = $this->query($type);
        $transactions=$query->whereDate('created_at', '>=', $start_date)
            ->whereDate('created_at', '<=', $end_date)
            ->where('status','Gagal')
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        return $this->checkNullTransaction($transactions,$start_date,$end_date);
    }

    public function allTransaction($start_date,$end_date,$status){
        $pulsas = TransactionPulsa::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->where('status','LIKE','%'.$status.'%')
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        $packetDatas = TransactionPacketData::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)->selectRaw('date(created_at) date')
            ->where('status','LIKE','%'.$status.'%')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        $emoneys = TransactionEmoney::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->where('status','LIKE','%'.$status.'%')
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        $prepaids = TransactionPlnPrepaid::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->where('status','LIKE','%'.$status.'%')
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        $postpaids = TransactionPlnPostpaid::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->where('status','LIKE','%'.$status.'%')
            ->selectRaw('date(created_at) date')
            ->selectRaw('count(created_at) as count')
            ->groupBy('date')
            ->get();
        return $this->loopingAllChartTransaction($start_date,$end_date,$pulsas,$packetDatas,$emoneys,$prepaids,$postpaids);
    }

    public function getAllTransaction($start_date,$end_date){
        $data=[
            'all'=>$this->allTransaction($start_date,$end_date,''),
            'success'=>$this->allTransaction($start_date,$end_date,'Sukses'),
            'failed'=>$this->allTransaction($start_date,$end_date,'Gagal'),
        ];
        return $data;
    }

    public function loopingAllChartTransaction($start_date,$end_date,$pulsas,$packetDatas,$emoneys,$prepaids,$postpaids){
        $start_date=Carbon::parse($start_date);
        $end_date=Carbon::parse($end_date);
        $diff = $start_date->diffInDays($end_date);
        $data = [];
        for($i = 0; $i <= $diff; $i++){
            $date=Carbon::parse($start_date)->addDays($i)->format('Y-m-d');
            $count=0;
            foreach($pulsas as $pulsa){
                if($date == $pulsa['date']){
                    $count+=$pulsa['count'];
                }
            }
            foreach($packetDatas as $packetData){
                if($date == $packetData['date']){
                    $count+=$packetData['count'];
                }
            }
            foreach($emoneys as $emoney){
                if($date == $emoney['date']){
                    $count+=$emoney['count'];
                }
            }
            foreach($prepaids as $prepaid){
                if($date == $prepaid['date']){
                    $count+=$prepaid['count'];
                }
            }
            foreach($postpaids as $postpaid){
                if($date == $postpaid['date']){
                    $count+=$postpaid['count'];
                }
            }
            $response=[
                'date'=>$date,
                'count'=>$count
            ];
            array_push($data,$response);
        }
        return $data;
    }

    public function getAllIncome($start_date,$end_date){
        $pulsas = TransactionPulsa::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->selectRaw('date(created_at) date')
            ->selectRaw('sum(admin_fee) as income')
            ->groupBy('date')
            ->get();
        $packetDatas = TransactionPacketData::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)->selectRaw('date(created_at) date')
            ->selectRaw('sum(admin_fee) as income')
            ->groupBy('date')
            ->get();
        $emoneys = TransactionEmoney::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->selectRaw('date(created_at) date')
            ->selectRaw('sum(admin_fee) as income')
            ->groupBy('date')
            ->get();
        $prepaids = TransactionPlnPrepaid::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->selectRaw('date(created_at) date')
            ->selectRaw('sum(admin_fee) as income')
            ->groupBy('date')
            ->get();
        $postpaids = TransactionPlnPostpaid::query()->whereDate('created_at','>=', $start_date)
            ->whereDate('created_at','<=', $end_date)
            ->selectRaw('date(created_at) date')
            ->selectRaw('sum(admin_fee) as income')
            ->groupBy('date')
            ->get();
        $start_date=Carbon::parse($start_date);
        $end_date=Carbon::parse($end_date);
        $diff = $start_date->diffInDays($end_date);
        $data = [];
        for($i = 0; $i <= $diff; $i++){
            $date=Carbon::parse($start_date)->addDays($i)->format('Y-m-d');
            $income=0;
            foreach($pulsas as $pulsa){
                if($date == $pulsa['date']){
                    $income+=$pulsa['income'];
                }
            }
            foreach($packetDatas as $packetData){
                if($date == $packetData['date']){
                    $income+=$packetData['income'];
                }
            }
            foreach($emoneys as $emoney){
                if($date == $emoney['date']){
                    $income+=$emoney['income'];
                }
            }
            foreach($prepaids as $prepaid){
                if($date == $prepaid['date']){
                    $income+=$prepaid['income'];
                }
            }
            foreach($postpaids as $postpaid){
                if($date == $postpaid['date']){
                    $income+=$postpaid['income'];
                }
            }
            $response=[
                'date'=>$date,
                'income'=>$income
            ];
            array_push($data,$response);
        }
        return $data;
    }

    public function getIncome(){
        $pulsa = TransactionPulsa::query()->where('status','Sukses')->sum('admin_fee');
        $packetData = TransactionPacketData::query()->where('status','Sukses')->sum('admin_fee');
        $emoney = TransactionEmoney::query()->where('status','Sukses')->sum('admin_fee');
        $prepaid = TransactionPlnPrepaid::query()->where('status','Sukses')->sum('admin_fee');
        $postpaid = TransactionPlnPostpaid::query()->where('status','Sukses')->sum('admin_fee');

        $income = $pulsa+$packetData+$emoney+$prepaid+$postpaid;
        return ['digital'=>$income, 'product'=>0];
    }
}
