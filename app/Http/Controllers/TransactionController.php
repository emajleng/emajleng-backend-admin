<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Denom;
use App\Models\Product;
use App\Models\Transaction;
use App\Models\TransactionEmoney;
use App\Models\TransactionPacketData;
use App\Models\TransactionPlnPostpaid;
use App\Models\TransactionPlnPrepaid;
use App\Models\TransactionPulsa;
use App\Models\User;
use App\Notifications\SuccessTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
class TransactionController extends Controller
{
    public function getTransactionDigital(Request $request){
        $validator = Validator::make($request->all(), [
            'page' => 'required|integer|min:0'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        if(!$request->type){
            return $this->getAllTransactionDigital($request->page,$request->ref_id);
        }
        if($request->type=='PULSA'){
            $data = TransactionPulsa::query()->where('ref_id','LIKE','%'.$request->ref_id.'%');
        }elseif($request->type=='DATA'){
            $data = TransactionPacketData::query()->where('ref_id','LIKE','%'.$request->ref_id.'%');
        }elseif($request->type=='EMONEY'){
            $data = TransactionEmoney::query()->where('ref_id','LIKE','%'.$request->ref_id.'%');
        }elseif($request->type=='PLN'){
            $data = TransactionPlnPrepaid::query()->where('ref_id','LIKE','%'.$request->ref_id.'%');
        }elseif($request->type=='PLN_POSTPAID'){
            $data = TransactionPlnPostpaid::query()->where('ref_id','LIKE','%'.$request->ref_id.'%');
        }
        $totalPage = ceil($data
        ->get()
        ->count() / 10);
        $transactions = $data->orderBy('created_at', 'desc')
        ->skip($request['page']*10)
        ->take(10)
        ->get();
        foreach($transactions as $transaction){
            $transaction['denom_id'] = Denom::find($transaction->denom_id);
            $transaction['user'] = $transaction->user;
            if($request->type=='PLN_POSTPAID'){
                $transaction['period'] = json_decode($transaction->period);
            }
        }
        return ResponseHelper::paging($transactions, $request['page'], $totalPage);
    }

    public function getAllTransactionDigital($page,$key){
        $pulsa = TransactionPulsa::query()
        ->where('ref_id','LIKE','%'.$key.'%')
        ->select(DB::raw('id'),
            DB::raw('denom_id'),
            DB::raw('user_id'),
            DB::raw('amount'),
            DB::raw('null as customer_name'),
            DB::raw('customer_number'),
            DB::raw('status'),
            DB::raw('client_response'),
            DB::raw('admin_fee'),
            DB::raw('null as tarif'),
            DB::raw('ref_id'),
            DB::raw('null as voltage'),
            DB::raw('null as token_pln'),
            DB::raw('null as period'),
            DB::raw('balance'),
            DB::raw('created_at'),
            DB::raw('updated_at'));
        $emoney = TransactionEmoney::query()
        ->where('ref_id','LIKE','%'.$key.'%')
        ->select(DB::raw('id'),
            DB::raw('denom_id'),
            DB::raw('user_id'),
            DB::raw('amount'),
            DB::raw('null as customer_name'),
            DB::raw('customer_number'),
            DB::raw('status'),
            DB::raw('client_response'),
            DB::raw('admin_fee'),
            DB::raw('null as tarif'),
            DB::raw('ref_id'),
            DB::raw('null as voltage'),
            DB::raw('null as token_pln'),
            DB::raw('null as period'),
            DB::raw('balance'),
            DB::raw('created_at'),
            DB::raw('updated_at'));
        $packetData = TransactionPacketData::query()
        ->where('ref_id','LIKE','%'.$key.'%')
        ->select(DB::raw('id'),
            DB::raw('denom_id'),
            DB::raw('user_id'),
            DB::raw('amount'),
            DB::raw('null as customer_name'),
            DB::raw('customer_number'),
            DB::raw('status'),
            DB::raw('client_response'),
            DB::raw('admin_fee'),
            DB::raw('null as tarif'),
            DB::raw('ref_id'),
            DB::raw('null as voltage'),
            DB::raw('null as token_pln'),
            DB::raw('null as period'),
            DB::raw('balance'),
            DB::raw('created_at'),
            DB::raw('updated_at'));
        $prepaid = TransactionPlnPrepaid::query()
        ->where('ref_id','LIKE','%'.$key.'%')
        ->select(DB::raw('id'),
            DB::raw('denom_id'),
            DB::raw('user_id'),
            DB::raw('amount'),
            DB::raw('customer_name'),
            DB::raw('customer_number'),
            DB::raw('status'),
            DB::raw('client_response'),
            DB::raw('admin_fee'),
            DB::raw('tarif'),
            DB::raw('ref_id'),
            DB::raw('voltage'),
            DB::raw('token_pln'),
            DB::raw('null as period'),
            DB::raw('balance'),
            DB::raw('created_at'),
            DB::raw('updated_at'));
        $query = TransactionPlnPostpaid::query()
        ->where('ref_id','LIKE','%'.$key.'%')
        ->select(DB::raw('id'),
            DB::raw('denom_id'),
            DB::raw('user_id'),
            DB::raw('amount'),
            DB::raw('customer_name'),
            DB::raw('customer_number'),
            DB::raw('status'),
            DB::raw('client_response'),
            DB::raw('admin_fee'),
            DB::raw('tarif'),
            DB::raw('ref_id'),
            DB::raw('voltage'),
            DB::raw('null as token_pln'),
            DB::raw('period'),
            DB::raw('balance'),
            DB::raw('created_at'),
            DB::raw('updated_at'))
        ->union($pulsa)
        ->union($emoney)
        ->union($packetData)
        ->union($prepaid);

        $totalPage = ceil($query
        ->get()
        ->count() / 10);
        $data = $query->orderBy('created_at', 'desc')
        ->skip($page*10)
        ->take(10)
        ->get();

        foreach($data as $transaction){
            $transaction['denom_id'] = Denom::find($transaction->denom_id);
            $transaction['user'] = $transaction->user;
            if($transaction->period){
                $transaction['period'] = json_decode($transaction->period);
            }
        }
        return ResponseHelper::paging($data, $page, $totalPage);
    }

    public function getTransactionProduct(Request $request){
        $query =  Transaction::query()
        ->where('status','LIKE','%'.$request->status.'%')
        ->where('unique_code','LIKE','%'.$request->uniqueCode.'%')
        ->orderBy('created_at','DESC');
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $transactions = $query->skip($request['page']*10)
            ->take(10)->get();
        foreach ($transactions as $transaction){
            $details = json_decode($transaction->detail);
            $product = [];
            // $data = Product::find($details[0]->id);
            foreach ($details as $detail) {
                $data = Product::find($detail->product_id);
                $data->photo = env('API_URL').$data->photo;
                array_push($product, $data);
            }
                $transaction['detail'] = $product;
                $transaction['user'] = $transaction->user;
                $transaction['payment_method'] = $transaction->payment_method;
                $transaction['origin_details'] = json_decode($transaction->origin_details);
                $transaction['destination_details'] = json_decode($transaction->destination_details);
                $transaction['payment_confirmation'] = env('API_URL').$transaction->payment_confirmation;
        }
        return ResponseHelper::paging($transactions,$request->page,$totalPage);
    }

    public function updateStatus(Request $request, $id){
        $validationType = array('REQUESTED','REJECTED', 'FINISHED');
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:' . implode(',', $validationType),
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
         $withdraw = Transaction::find($id);
         $curentStatus = $withdraw->status;
         $withdraw->status = $request->status;
         if($curentStatus=='WAITING_PAYMENT' && ($request->status=='REQUESTED' || $request->status=='REJECTED')){
             $withdraw->save();
         }else if($curentStatus=='DELIVERING' && $request->status=='FINISHED'){
             $withdraw->save();
         }else{
            return ResponseHelper::badRequest('Incorent Status', "Incorent Status");
         }
        
        return ResponseHelper::ok(true);
    }
}
