<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\City;
use App\Models\Districts;
use App\Models\News;
use App\Models\ProductCategory;
use App\Models\Province;
use App\Models\Village;

class PublicController extends Controller
{
    public function getProvince(){
        $data = Province::query()->get();
        return ResponseHelper::ok($data);
    }

    public function getProductCategory(){
        $data = ProductCategory::query()->get();
        return ResponseHelper::ok($data);
    }

    public function getNews(){
        $news = News::query()->get();
        $data=[];
        foreach ($news as $news){
            $response=[
                'id' => $news->id,
                'title' => $news->title,
                'content' => $news->content,
                'image' => url($news->image),
                'created_at' => $news->created_at,
                'updated_at' => $news->updated_at,
            ];
            array_push($data,$response);
        }
        return ResponseHelper::ok($data);
    }

    public function getCity($id){
        $data = City::query()->where('province_id',$id)->get();
        return ResponseHelper::ok($data);
    }

    public function getDistrict($id){
        $data = Districts::query()->where('city_id',$id)->get();
        return ResponseHelper::ok($data);
    }

    public function getVillage($id){
        $data = Village::query()->where('district_id',$id)->get();
        return ResponseHelper::ok($data);
    }
}
