<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Helper\ResponseHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public function createNews(Request $request){
        $validator = Validator::make($request->all(), [
            'image' => 'required',
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        $file = $request->image;
        $image_path = 'storage/image/news/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);
        News::create([
            'image' => $filename,
            'title' => $request->title,
            'content' => $request->content,
        ]);
        return ResponseHelper::ok(true);
    }

    public function getNews(Request $request){
        $skip_page = $request['page'] * 10;
        $total_page = null;
        $query = News::query()->orderBy('created_at','DESC');
        $news = $query->skip($skip_page)->take(10)->get();
        $total_page = ceil($query->count()/10);
        foreach ($news as $new) {
            $new['image'] = url($new->image);
        }
        return ResponseHelper::paging($news,$request['page'], $total_page);
    }

    public function getNewsById(Request $request, $id){
        $news = News::find($id);
        return ResponseHelper::ok($news);
    }

    public function updateNews(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $data = News::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($request->image){
            if($data->image){
                unlink(base_path('public/').$data->image);
            }
            $file = $request->image;
            $image_path = 'storage/image/news/';
            $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
            $file->move($image_path, $filename);
            $data->image=$filename;
        }
        $data->title=$request->title;
        $data->content=$request->content;
        $data->save();
        return ResponseHelper::ok(true);
    }

    public function deleteNews($id)
    {
        $data = News::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($data->image){
            unlink(base_path('public/').$data->image);
        }
        $data->delete();
        return ResponseHelper::ok(true);
    }
}
