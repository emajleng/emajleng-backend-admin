<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Merchant;
use App\Models\Withdraw;
use App\Models\UserBank;
use App\Models\User;
// use App\Notifications\SuccessTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class WithdrawController extends Controller
{

    public function getWithdraw(Request $request){
        $query =  Withdraw::query();
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $withdraws = $query->skip($request['page']*10)
            ->take(10)->get();
        foreach($withdraws as $withdraw){
            $withdraw['id'] = $withdraw->id;
            $withdraw['amount'] = $withdraw->amount;
            $withdraw['status'] = $withdraw->status;
            $withdraw['created_at'] = $withdraw->created_at;
            $withdraw['user_bank'] = $withdraw->userBank->user;
        }
        return ResponseHelper::paging($withdraws, $request['page'], $totalPage);
    }

    public function updateStatus(Request $request, $id){
        $validationType = array('APPROVED','REJECTED');
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:' . implode(',', $validationType),
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        DB::beginTransaction();
        try{
            $withdraw = Withdraw::find($id);
            $withdraw->status = $request->status;
            $withdraw->save();
            if($request->status=='REJECTED'){
                $user = User::find($withdraw->userBank->user->id);
                $user->balance += ($withdraw->amount);
                $user->save();
            }
        }catch (Exception $e) {
            DB::rollBack();
            return ResponseHelper::serviceUnavailable('Internal server error');
        }
        DB::commit();
        return ResponseHelper::ok(true);
    }
}
