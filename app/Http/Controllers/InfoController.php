<?php


namespace App\Http\Controllers;

use App\Models\Denom;
use Illuminate\Http\Request;
use App\Helper\Client;
use App\Models\TransactionPulsa;
use App\Models\TransactionEmoney;
use App\Models\TransactionPacketData;
use App\Models\TransactionPlnPostpaid;
use App\Models\TransactionPlnPrepaid;
use App\Helper\ResponseHelper;
use Carbon\Carbon;

class InfoController extends Controller{
    public function index(){
        $pulsas = TransactionPulsa::query()->select('denom_id')
            ->selectRaw('count(denom_id) as count')
            ->groupBy('denom_id')
            ->get();
        $packetDatas = TransactionPacketData::query()->select('denom_id')
            ->selectRaw('count(denom_id) as count')
            ->groupBy('denom_id')
            ->get();
        $emoneys = TransactionEmoney::query()->select('denom_id')
            ->selectRaw('count(denom_id) as count')
            ->groupBy('denom_id')
            ->get();
        $prepaids = TransactionPlnPrepaid::query()->select('denom_id')
            ->selectRaw('count(denom_id) as count')
            ->groupBy('denom_id')
            ->get();
        $postpaids = TransactionPlnPostpaid::query()->select('denom_id')
            ->selectRaw('count(denom_id) as count')
            ->groupBy('denom_id')
            ->get();

        $denoms = Denom::query()->get();
        $data=[];

        foreach ($denoms as $denom){
            $count=0;
            foreach($pulsas as $pulsa){
                if($denom->id == $pulsa['denom_id']){
                    $count+=$pulsa['count'];
                }
            }
            foreach($packetDatas as $packetData){
                if($denom->id == $packetData['denom_id']){
                    $count+=$packetData['count'];
                }
            }
            foreach($emoneys as $emoney){
                if($denom->id == $emoney['denom_id']){
                    $count+=$emoney['count'];
                }
            }
            foreach($prepaids as $prepaid){
                if($denom->id == $prepaid['denom_id']){
                    $count+=$prepaid['count'];
                }
            }
            foreach($postpaids as $postpaid){
                if($denom->id == $postpaid['denom_id']){
                    $count+=$postpaid['count'];
                }
            }
            $response=[
                'code'=>$denom->code,
                'count'=>$count
            ];
            array_push($data,$response);
        }
        $response = collect($data);
        $sortDenom = $response->sortByDesc('count');
        return $sortDenom->values()->all();
    }
}
