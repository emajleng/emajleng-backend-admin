<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Denom;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DenomController extends Controller
{
    public function index(){
        $denoms = Denom::query()->get();
        $data = [];
        foreach($denoms as $denom){
            $response=[
                'id' => $denom->id,
                'provider' => [
                    'name' => $denom->provider->name,
                    'logo' => url($denom->provider->logo)
                ],
                'amount' => $denom->amount,
                'code' => $denom->code,
                'price' => $denom->price,
                'admin_fee' => $denom->admin_fee,
                'status' => $denom->status,
            ];
            array_push($data, $response);
        }
        return ResponseHelper::ok($data);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'status' => 'required',
            'admin_fee' => 'required|integer',
            'price' => 'required|integer',
            'amount' => 'required',
            'provider_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $denom = Denom::create([
            'provider_id'=>$request->provider_id,
            'admin_fee'=>$request->admin_fee,
            'status'=>$request->status,
            'price'=>$request->price,
            'amount'=>$request->amount,
            'code'=>$request->code,
        ]);
        return ResponseHelper::ok(true);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'status' => 'required',
            'admin_fee' => 'required|integer',
            'price' => 'required|integer',
            'amount' => 'required',
            'provider_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $denom = Denom::where('id',$id)->update([
            'provider_id'=>$request->provider_id,
            'admin_fee'=>$request->admin_fee,
            'status'=>$request->status,
            'price'=>$request->price,
            'amount'=>$request->amount,
            'code'=>$request->code,
        ]);
        return ResponseHelper::ok(true);
    }

    public function destroy($id)
    {
        $data = Denom::find($id);
        $data->delete();
        return ResponseHelper::ok(true);
    }
}
