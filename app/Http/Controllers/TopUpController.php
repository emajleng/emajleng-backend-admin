<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Merchant;
use App\Models\TopUp;
use App\Models\User;
// use App\Notifications\SuccessTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class TopUpController extends Controller
{

    public function getTopUp(Request $request){
        $data = TopUp::query()->where('unique_code','LIKE','%'.$request->unique_code.'%');
        $totalPage = ceil($data
        ->get()
        ->count() / 10);
        $topups = $data->orderBy('created_at', 'desc')
        ->skip($request['page']*10)
        ->take(10)
        ->get();
        foreach($topups as $topup){
            $topup['user'] = $topup->user;
            $topup['payment_method'] = $topup->paymentMethod;
        }
        return ResponseHelper::paging($topups, $request['page'], $totalPage);
    }

    public function updateTopUp(Request $request, $id){
        $validationType = array('PAID','CANCEL');
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:' . implode(',', $validationType),
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        DB::beginTransaction();
        try{
            $topUp = TopUp::find($id);
            $statusTopUP = $topUp->status;
            $topUp->status = $request->status;
            $topUp->save();
            if($request->status=='PAID'){
                $user = User::find($topUp->user_id);
                $user->balance += ($topUp->amount+$topUp->unique_code);
                $user->save();
            }else if($statusTopUP=='PAID'){
                $user = User::find($topUp->user_id);
                $user->balance -= ($topUp->amount+$topUp->unique_code);
                $user->save();
            }
            $data = [
                "amount"=>$topUp->amount,
                "status"=>$request->status=='PAID'?'Berhasil':'Gagal'
            ];
            $user = User::find($topUp->user_id);
            // $user->notify(new SuccessTransaction($data));
        }catch (Exception $e) {
            DB::rollBack();
            return ResponseHelper::serviceUnavailable('Internal server error');
        }
        DB::commit();
        return ResponseHelper::ok(true);
    }

    public function createTopUp(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer|exists:users,id',
            'payment_method_id' => 'required|integer|exists:payment_methods,id',
            'amount' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        DB::beginTransaction();
        try{
            $topUp = TopUp::query()->create([
                'user_id' => $request->user_id,
                'payment_method_id' => $request->payment_method_id,
                'amount' => $request->amount,
                'status' => 'PAID',
                'unique_code' => 0,
            ]);
            $user = User::query()->find($topUp->user_id);
            $user->balance += $topUp->amount;
            $user->save();
            $data = [
                "amount"=>$topUp->amount,
                "status"=>'Berhasil'
            ];
            $user = User::query()->find($topUp->user->user_id);
            // $user->notify(new SuccessTransaction($data));
        }catch (Exception $e) {
            DB::rollBack();
            return ResponseHelper::serviceUnavailable('Internal server error');
        }
        DB::commit();
        return ResponseHelper::ok(true);
    }
}
