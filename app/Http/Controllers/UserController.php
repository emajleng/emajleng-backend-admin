<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\User;
use App\Models\Merchant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function getUser(Request $request){
        $skip_page = $request['page'] * 10;
        $total_page = null;
        $query = User::query()->orWhere('name','LIKE','%'.$request->search.'%')->orWhere('phone','LIKE','%'.$request->search.'%')->orderBy('created_at','DESC');
        $users = $query->skip($skip_page)->take(10)->get();
        foreach($users as $user){
            if($user->merchant){
                $user['merchant'] = $user->merchant;
                $user['merchant']['province'] = $user->merchant->province;
                $user['merchant']['city'] = $user->merchant->city;
                $user['merchant']['district'] = $user->merchant->districts;
                $user['merchant']['village'] = $user->merchant->village;
            }
        }
        $total_page = ceil($query->count()/10);
        return ResponseHelper::paging($users,$request['page'], $total_page);
    }

    public function createUser(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users',
            'email' => 'required|unique:users',
            'address' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        User::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => app('hash')->make(env("DEFAULT_PASSWORD")),
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
        ]);
        $user = [
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => env("DEFAULT_PASSWORD"),
        ];
        return ResponseHelper::ok($user);
    }

    public function createMerchant(Request $request, $id){
        if(User::find($id)->merchant){
            return ResponseHelper::badRequest(['Anda sudah membuat merchant'],'you already have merchant');
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'logo' => 'required|image',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $file = $request->logo;
        $image_path = 'storage/image/merchant/logo/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);
        User::find($id)->merchant()->create([
            'name' => $request->name,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
            'logo' => $filename,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude
        ]);
        return ResponseHelper::ok(true);
    }

    public function getMerchant(Request $request){
        $skip_page = $request['page'] * 10;
        $total_page = null;
        $query = Merchant::query()->orWhere('name','LIKE','%'.$request->search.'%')->orderBy('created_at','DESC');
        $merchants = $query->skip($skip_page)->take(10)->get();
        foreach($merchants as $merchant){
                $merchant['name'] = $merchant->name;
                $merchant['logo'] = env('API_URL').$merchant->logo;
                $merchant['user'] = $merchant->user;
                $merchant['province'] = $merchant->province;
                $merchant['city'] = $merchant->city;
                $merchant['district'] = $merchant->districts;
                $merchant['village'] = $merchant->village;
                $merchant['latitude'] = $merchant->latitude;
                $merchant['longitude'] = $merchant->longitude;
        };
        $total_page = ceil($query->count()/10);
        return ResponseHelper::paging($merchants,$request['page'], $total_page);
    }

    public function updateUser(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $user = User::find($id);
        if (!$user) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        User::where('id',$id)->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
        ]);
        return ResponseHelper::ok(true);
    }

    public function updateMerchant(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $filename = auth()->user()->merchant->logo;
        if($request->logo){
            $file = $request->logo;
            $image_path = 'storage/image/merchant/logo/';
            $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
            $file->move($image_path, $filename);
        }
        auth()->user()->merchant()->update([
            'name' => $request->name,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'district_id' => $request->district_id,
            'village_id' => $request->village_id,
            'logo' => $filename,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude
        ]);
        return ResponseHelper::ok(true);
    }

    public function updateStatusMerchant(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $merchant = Merchant::query()->find($id);
        if(!$merchant){
            return ResponseHelper::badRequest("invalid id", "Validation required");
        }
        $merchant->status = $request->status;
        $merchant->save();
        return ResponseHelper::ok(true);
    }

    public function updateStatusSiupMerchant(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'status' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $merchant = Merchant::query()->find($id);
        if(!$merchant){
            return ResponseHelper::badRequest("invalid id", "Validation required");
        }
        $merchant->siup_status = $request->status;
        $merchant->save();
        return ResponseHelper::ok(true);
    }
}
