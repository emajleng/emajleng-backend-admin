<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\AdminFee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminFeeController extends Controller
{
    public function index(){
        $fee = AdminFee::query()->get();
        return ResponseHelper::ok($fee);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'price' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $fee = AdminFee::where('id',$id)->update([
            'price'=>$request->price,
        ]);
        return ResponseHelper::ok(true);
    }
}
