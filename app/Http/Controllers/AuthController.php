<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(){
        $credentials = request(['email', 'password']);
        if (! $token = app('auth')->attempt($credentials)) {
            return ResponseHelper::unauthorized("Email atau password yang anda masukan salah!");
        }
        $response = [
            'token' => $token,
            'token_type' => 'bearer',
        ];
        return ResponseHelper::ok($response);
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        Admin::query()->create([
            'name' => $request->name,
            'email' => $request->email,
            'password'=>app('hash')->make($request->password)
        ]);
        return ResponseHelper::ok(true);
    }

    public function logout(){
        app('auth')->logout();
        return ResponseHelper::ok(true);
    }
}
