<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Banner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class BannerController extends Controller
{
    public function index(){
        $banners = Banner::query()->get();
        $data = [];
        foreach($banners as $banner){
            $response=[
                'id' => $banner->id,
                'image' => url($banner->image),
                'description' => $banner->description,
            ];
            array_push($data, $response);
        }
        return ResponseHelper::ok($data);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            'image' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $file = $request->image;
        $image_path = 'storage/image/banner/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);
        $banner = Banner::create([
            'description'=>$request->description,
            'image'=>$filename,
        ]);
        return ResponseHelper::ok(true);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            // 'image' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $data = Banner::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($request->image){
            if($data->image){
                unlink(base_path('public/').$data->image);
            }
            $file = $request->image;
            $image_path = 'storage/image/banner/';
            $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
            $file->move($image_path, $filename);
            $data->image=$filename;

        }
        $data->description = $request->description;
        $data->save();
        return ResponseHelper::ok(true);
    }

    public function destroy($id)
    {
        $data = Banner::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($data->image){
            unlink(base_path('public/').$data->image);
        }
        $data->delete();
        return ResponseHelper::ok(true);
    }
}
