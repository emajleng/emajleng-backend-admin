<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\Product;
use App\Models\Merchant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function getAllProduct(Request $request){
        $query =  Product::query()
            ->where('name','LIKE','%'.$request->name.'%')
            ->where('stock','>',0)
            ->where('status',true);
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $products = $query->skip($request['page']*10)
            ->take(10)->get();
        $data=[];
        foreach ($products as $product){
            $response=[
                'id' => $product->id,
                'productCategory' => $product->productCategory,
                'name' => $product->name,
                'price' => $product->price,
                'stock' => $product->stock,
                'weight' => $product->weight,
                'description' => $product->description,
                'photo' => env('API_URL').$product->photo,
                'created_at' => $product->created_at,
                'updated_at' => $product->updated_at,
            ];
            array_push($data,$response);
        }
        return ResponseHelper::paging($data,$request->page,$totalPage);
    }

    public function getProductMerchant(Request $request, $id){
        $query =  Merchant::find($id)->product()
            ->where('name','LIKE','%'.$request->name.'%')
            ->where('stock','>',0)
            ->where('status',true);
        $totalPage = ceil($query
                ->get()
                ->count() / 10);
        $products = $query->skip($request['page']*10)
            ->take(10)->get();
        $data=[];
        foreach ($products as $product){
            $response=[
                'id' => $product->id,
                'productCategory' => $product->productCategory,
                'name' => $product->name,
                'price' => $product->price,
                'stock' => $product->stock,
                'description' => $product->description,
                'photo' => env('API_URL').$product->photo,
                'created_at' => $product->created_at,
                'updated_at' => $product->updated_at,
            ];
            array_push($data,$response);
        }
        return ResponseHelper::paging($data,$request->page,$totalPage);
    }

    public function inActiveProduct($id){
        auth()->user()->merchant->product()->where('id',$id)->first()->update([
            'status' => false,
        ]);
        return ResponseHelper::ok(true);
    }
}
