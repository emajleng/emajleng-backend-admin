<?php

namespace App\Http\Controllers;

use App\Helper\Client;
use App\Helper\ResponseHelper;
use App\Models\Provider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProviderController extends Controller
{
    public function index(){
        $data = [];
        $urlData=[
            "commands"=> 'prepaid',
            'username'=>env('UsernameDigiflazz'),
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').'pricelist')
        ];
        $digiflazz = Client::digiflazz($urlData,'price-list');
        $providers = Provider::query()->get();
        foreach($providers as $provider){
            $denoms=[];
            foreach($provider->denom as $denom){
                $price=null;
                foreach($digiflazz->data as $digi){
                    if($denom->code==$digi->buyer_sku_code){
                        $price=$digi->price;
                    }
                }
                $responseDenom=[
                    'id'=>$denom->id,
                    'amount'=>$denom->amount,
                    'code'=>$denom->code,
                    'admin_fee'=>$denom->admin_fee,
                    'status'=>$denom->status,
                    'price'=>$price,
                ];
                array_push($denoms, $responseDenom);
            }
            $response =[
                'id' => $provider->id,
                'name' => $provider->name,
                'logo' => url($provider->logo),
                'denom' => $denoms
            ];
            array_push($data, $response);
        }
        return ResponseHelper::ok($data);
    }

    public function getProviderById($id){
        $urlData=[
            "commands"=> 'prepaid',
            'username'=>env('UsernameDigiflazz'),
            'sign'=>md5(env('UsernameDigiflazz').env('KeyDigiflazz').'pricelist')
        ];
        $digiflazz = Client::digiflazz($urlData,'price-list');
        $provider = Provider::find($id);
        $denoms=[];
        foreach($provider->denom as $denom){
            $price=null;
            foreach($digiflazz->data as $digi){
                if($denom->code==$digi->buyer_sku_code){
                    $price=$digi->price;
                }
            }
            $responseDenom=[
                'id'=>$denom->id,
                'amount'=>$denom->amount,
                'code'=>$denom->code,
                'admin_fee'=>$denom->admin_fee,
                'status'=>$denom->status,
                'price'=>$price,
            ];
            array_push($denoms, $responseDenom);
        }
        $data =[
            'id' => $provider->id,
            'name' => $provider->name,
            'logo' => url($provider->logo),
            'denom' => $denoms
        ];
        return ResponseHelper::ok($data);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'logo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $file = $request->logo;
        $image_path = 'storage/image/provider/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);
        $provider = Provider::create([
            'name'=>$request->name,
            'logo'=>$filename
        ]);
        return ResponseHelper::ok(true);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            // 'logo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $data = Provider::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($request->logo){
            if($data->logo){
                unlink(base_path('public/').$data->logo);
            }
            $file = $request->logo;
            $image_path = 'storage/image/provider/';
            $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
            $file->move($image_path, $filename);
            $data->logo=$filename;
        }
        $data->name=$request->name;
        $data->save();
        return ResponseHelper::ok(true);
    }

    public function destroy($id)
    {
        $data = Provider::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($data->logo){
            unlink(base_path('public/').$data->logo);
        }
        $data->delete();
        return ResponseHelper::ok(true);
    }
}
