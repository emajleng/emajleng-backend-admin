<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use App\Helper\ResponseHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    public function createCategory(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }

        ProductCategory::create([
            'name' => $request->name,
        ]);
        return ResponseHelper::ok(true);
    }

    public function getCategory(Request $request){
        $skip_page = $request['page'] * 10;
        $total_page = null;
        $query = ProductCategory::query()->orWhere('name','LIKE','%'.$request->search.'%')->orderBy('created_at','DESC');
        $categorys = $query->skip($skip_page)->take(10)->get();
        $total_page = ceil($query->count()/10);
        return ResponseHelper::paging($categorys,$request['page'], $total_page);
    }

    public function updateCategory(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $category = ProductCategory::find($id);
        if (!$category) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        ProductCategory::where('id',$id)->update([
            'name' => $request->name,
        ]);
        return ResponseHelper::ok(true);
    }

    public function inActiveCategory($id){
        ProductCategory::where('id',$id)->first()->update([
            'status' => false,
        ]);
        return ResponseHelper::ok(true);
    }
}
