<?php

namespace App\Http\Controllers;

use App\Helper\Client;
use App\Helper\ResponseHelper;
use App\Models\Courier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourierController extends Controller
{
    public function index(){
        $couriers = Courier::query()->get();
        foreach ($couriers as $courier) {
            $courier['logo'] = url($courier->logo);
        }
        return ResponseHelper::ok($couriers);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'code' => 'required|string',
            'logo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $file = $request->logo;
        $image_path = 'storage/image/courier/';
        $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
        $file->move($image_path, $filename);
        $courier = Courier::create([
            'name'=>$request->name,
            'code'=>$request->code,
            'logo'=>$filename
        ]);
        return ResponseHelper::ok(true);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'code' => 'required|string',
            // 'logo' => 'required|image',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $data = Courier::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($request->logo){
            if($data->logo){
                unlink(base_path('public/').$data->logo);
            }
            $file = $request->logo;
            $image_path = 'storage/image/courier/';
            $filename = $image_path.Carbon::now()->unix() . '.' . $file->getClientOriginalExtension();
            $file->move($image_path, $filename);
            $data->logo=$filename;
        }
        $data->name=$request->name;
        $data->code=$request->code;
        $data->save();
        return ResponseHelper::ok(true);
    }

    public function destroy($id)
    {
        $data = Courier::find($id);
        if (!$data) {
            return ResponseHelper::badRequest(['Data tidak ditemukan'], "Validation required");
        }
        if($data->logo){
            unlink(base_path('public/').$data->logo);
        }
        $data->delete();
        return ResponseHelper::ok(true);
    }
}
