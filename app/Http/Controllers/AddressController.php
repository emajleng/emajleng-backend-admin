<?php

namespace App\Http\Controllers;

use App\Models\Province;
use App\Models\City;
use App\Models\Districts;
use App\Models\Village;
use App\Helper\ResponseHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{

    public function getProvince(){
        $data = Province::get();
        return ResponseHelper::ok($data);
    }
    public function getCity($id){
        $data = City::query()->where('province_id',$id)->get();
        return ResponseHelper::ok($data);
    }
    public function getDistrict($id){
        $data = Districts::query()->where('city_id',$id)->get();
        return ResponseHelper::ok($data);
    }
    public function getVillage($id){
        $data = Village::query()->where('district_id',$id)->get();
        return ResponseHelper::ok($data);
    }
}
