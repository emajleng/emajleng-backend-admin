<?php

namespace App\Http\Controllers;

use App\Helper\ResponseHelper;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentMethodController extends Controller
{
    public function index(){
        $data = PaymentMethod::query()->get();
        return ResponseHelper::ok($data);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required|string',
            'account_number' => 'required|numeric',
            'account_name' => 'required|string',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $menu = PaymentMethod::create([
            'bank_name'=>$request->bank_name,
            'account_number'=>$request->account_number,
            'account_name'=>$request->account_name
        ]);
        return ResponseHelper::ok(true);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'bank_name' => 'required|string',
            'account_number' => 'required|numeric',
            'account_name' => 'required|string',
        ]);
        if ($validator->fails()) {
            return ResponseHelper::badRequest($validator->errors()->all(), "Validation required");
        }
        $menu = PaymentMethod::where('id',$id)->update([
            'bank_name'=>$request->bank_name,
            'account_number'=>$request->account_number,
            'account_name'=>$request->account_name
        ]);
        return ResponseHelper::ok(true);
    }

    public function destroy($id)
    {
        $data = PaymentMethod::find($id);
        $data->delete();
        return ResponseHelper::ok(true);
    }
}
