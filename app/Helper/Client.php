<?php

namespace App\Helper;
use Illuminate\Support\Facades\Http;

class Client{
    public static function postOtp($phone, $otp)
    {
        $data=[
            'userkey' => env('UserKey'),
            'passkey' => env('ApiKey'),
            'nohp' => $phone,
            'pesan' => "Kode OTP anda adalah ".$otp
        ];
        $sendOtp = Http::post(env('UrlOtp'), $data);
        $response = json_decode($sendOtp->body());
        return $response;
    }

    public static function digiflazz($data,$url)
    {
        $digiflazz = Http::post(env('UrlDigiflazz').$url, $data);
        $response = json_decode($digiflazz->body());
        return $response;
    }
}
