<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'id',
        'name',
    ];

    public function city()
    {
        return $this->hasMany('App\Models\City', 'province_id');
    }

    public function merchant()
    {
        return $this->hasMany('App\Models\Merchant', 'province_id');
    }

    public function user()
    {
        return $this->hasMany('App\Models\User', 'province_id');
    }
}
