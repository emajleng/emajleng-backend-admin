<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    protected $fillable = [
        'id',
        'city_id',
        'name',
    ];

    public function village()
    {
        return $this->hasMany('App\Models\Village', 'district_id');
    }

    public function merchant()
    {
        return $this->hasMany('App\Models\Merchant', 'district_id');
    }

    public function user()
    {
        return $this->hasMany('App\Models\User', 'district_id');
    }

    public function city(){
        return $this->belongsTo('App\Models\City','city_id','id');
    }
}
