<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';

    protected  $fillable = [
        'name'
    ];

    protected $hidden = [
        'updated_at', 'created_at'
    ];

    public function product()
    {
        return $this->hasMany('App\Models\Product', 'product_category_id');
    }
}
