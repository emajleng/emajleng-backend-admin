<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $table = 'couriers';

    protected $fillable = [
        'name', 'code', 'logo'
    ];

    public function CourierService()
    {
        return $this->hasMany('App\Models\CourierService', 'courier_id');
    }
}
