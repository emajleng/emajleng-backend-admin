<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $fillable = [
        'user_id',
        'payment_method_id',
        'courier_service_id',
        'amount',
        'unique_code',
        'detail',
        'status',
        'postal_fee',
        'delivery_time',
        'payment_expired',
        'airway_bill'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function payment_method(){
        return $this->belongsTo('App\Models\PaymentMethod','payment_method_id','id');
    }

    public function courierService(){
        return $this->belongsTo('App\Models\CourierService','courier_service_id','id');
    }
}
