<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, HasFactory;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'village_id',
        'district_id',
        'city_id',
        'province_id',
        'balance',
        'password',
        'fcm_token',
        'latest_token'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','fcm_token','latest_token', 'province_id','city_id','district_id', 'village_id', 'updated_at'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function cart()
    {
        return $this->hasMany('App\Models\Cart', 'user_id');
    }

    public function topUp()
    {
        return $this->hasMany('App\Models\TopUp', 'user_id');
    }

    public function province(){
        return $this->belongsTo('App\Models\Province','province_id','id');
    }

    public function city(){
        return $this->belongsTo('App\Models\City','city_id','id');
    }

    public function districts(){
        return $this->belongsTo('App\Models\Districts','district_id','id');
    }

    public function village(){
        return $this->belongsTo('App\Models\Village','village_id','id');
    }

    public function merchant()
    {
        return $this->hasOne('App\Models\Merchant', 'user_id');
    }

    public function userBank()
    {
        return $this->hasMany('App\Models\UserBank', 'user_id');
    }

    public function ppobTransaction()
    {
        return $this->hasMany('App\Models\ppobTransaction', 'user_id');
    }

    public function transaction()
    {
        return $this->hasMany('App\Models\transaction', 'user_id');
    }
}
