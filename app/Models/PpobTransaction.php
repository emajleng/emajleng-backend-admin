<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PpobTransaction extends Model
{
    protected $table = 'ppob_transactions';
    protected $fillable = [
        'user_id',
        'type',
        'provider',
        'denom',
        'status',
        'amount'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
